cask 'font-monaco-for-powerline' do
  version :latest
  sha256 :no_check

  url 'https://gitlab.com/sporkd/homebrew-fonts/-/archive/master/homebrew-fonts-master.zip?path=fonts'
  name 'Monaco for Powerline'
  homepage 'https://gitlab.com/sporkd/homebrew-fonts'

  font 'homebrew-fonts-master-fonts/fonts/monaco-for-powerline/Monaco for Powerline.ttf'
end
