cask 'font-meslo-lgs-nf' do
  version :latest
  sha256 :no_check

  url 'https://gitlab.com/sporkd/homebrew-fonts/-/archive/master/homebrew-fonts-master.zip?path=fonts'
  name 'MesloLGS NF'
  homepage 'https://gitlab.com/sporkd/homebrew-fonts'

  font 'homebrew-fonts-master-fonts/fonts/meslo-lgs-nf/MesloLGS NF Regular.ttf'
  font 'homebrew-fonts-master-fonts/fonts/meslo-lgs-nf/MesloLGS NF Bold.ttf'
  font 'homebrew-fonts-master-fonts/fonts/meslo-lgs-nf/MesloLGS NF Italic.ttf'
  font 'homebrew-fonts-master-fonts/fonts/meslo-lgs-nf/MesloLGS NF Bold Italic.ttf'
end
