# sporkd-fonts

A Homebrew Tap for storing some fonts I use locally.


## Installing fonts

After you've installed `homebrew` and `homebrew-cask`:

```bash
$ brew tap sporkd/fonts git@github.com:sporkd/homebrew-fonts.git
$ brew cask install sporkd/fonts/font-[name]
```
